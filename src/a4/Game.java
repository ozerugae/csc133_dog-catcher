package a4;

import a4.commands.*;
import a4.guiparts.CommandsPanel;
import a4.guiparts.MapView;
import a4.guiparts.MenuBar;
import a4.guiparts.ScoreView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Game class; holds a reference to the GameWorld
 * accepts user commands and calls the appropriate methods in GameWorld
 * Is the Controller.
 *
 * @see GameWorld
 */
public class Game extends JFrame implements ActionListener{
    private GameWorld gw;
    private MapView mv;
    private ScoreView sv;
    private Timer timer = new Timer(20,this);
    private static volatile Game theGame = null;
    private boolean isPaused = false;

    /**
     * Game constructor; creates the GameWorld, and the GUI
     */
    public Game() {
        ToggleGameState.setTarget(this);
        ToggleGameState.getInstance().setEnabled(true);

        gw = GameWorld.getInstance();   //Gameworld instance creation / singleton
        mv = new MapView(this);             //Map panel
        sv = new ScoreView();           //Score panel
        gw.addObserver(mv);             //map panel observes gameworld
        gw.addObserver(sv);             //score panel observes gameworld
        gw.initLayout();                //setup gameworld

        setTitle("Dog Catcher");        //title of window
        setSize(800, 600);              //size of window

        //Change window close behavior
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //We will use a quit command prompt instead
        //http://stackoverflow.com/a/6084070
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                Quit.getInstance().actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "quit"));
            }
        });

        /*
            Layout of the game window
         */
        setLayout(new BorderLayout());  //Border layout
        setJMenuBar(new MenuBar());   //Set menubar
        add(sv, BorderLayout.NORTH);    //scoreview up top
        JPanel ctrls = new CommandsPanel();
        add(ctrls, BorderLayout.WEST);  //command buttons on left
        add(mv, BorderLayout.CENTER);   //map on bottom right / center
        setVisible(true);
        gw.modelUpdated();
        timer.start();
    }

    /**
     * Invoked when an action occurs.
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Tick.getInstance().actionPerformed(e);
    }

    /**
     * Stop the game clock/timer in order to stop animation
     */
    public void stopAnimation(){
        timer.stop();
        Heal.getInstance().setEnabled(true);
        Scoop.getInstance().setEnabled(false);
        Contract.getInstance().setEnabled(false);
        Expand.getInstance().setEnabled(false);
        MoveDown.getInstance().setEnabled(false);
        MoveLeft.getInstance().setEnabled(false);
        MoveRight.getInstance().setEnabled(false);
        MoveUp.getInstance().setEnabled(false);
        isPaused = true;
    }


    /**
     * Restart the game clock/timer
     */
    public void resumeAnimation() {
        timer.restart();
        gw.resetStartTime();
        Heal.getInstance().setEnabled(false);
        Scoop.getInstance().setEnabled(true);
        Contract.getInstance().setEnabled(true);
        Expand.getInstance().setEnabled(true);
        MoveDown.getInstance().setEnabled(true);
        MoveLeft.getInstance().setEnabled(true);
        MoveRight.getInstance().setEnabled(true);
        MoveUp.getInstance().setEnabled(true);
        isPaused = false;
    }

    /**
     * Singleton for Game
     */
    public static Game getInstance(){
        if (theGame == null) {
            theGame = new Game();
        }
        return theGame;
    }

    /**
     * check if gamestate is paused
     * @return
     */
    public boolean isPaused(){
        return isPaused;
    }

    /**
     * call repaint on MapView
     */
    public void repaintMap(){
        mv.repaint();
    }
}
