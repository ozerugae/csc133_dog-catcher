package a4;

import a4.commands.*;
import a4.gameobjects.*;
import a4.sound.SoundHandler;

import javax.swing.*;
import java.util.*;

/**
 * GameWorld class; manipulates the objects in the GameWorld.
 * Is the model
 */
public class GameWorld extends Observable {
    private static volatile GameWorld theGameWorldInstance = null;
    public static final float width = 660, height = 481;
    private GameObjectCollection gameObjects = new GameObjectCollection();
    private int catsCaptured, dogsCaptured, catsRemaining, dogsRemaining, totalPoints;
    private Net theNet;
    private boolean sound = false; //default - sound off (checkbox is also off in menubar)
    private long startTime = System.nanoTime();
    private boolean alreadyStarted = false;


    private GameWorld() {
        //create and set target for AbstractActions (commands)
        Contract.setTarget(this);
        Expand.setTarget(this);
        MoveDown.setTarget(this);
        MoveLeft.setTarget(this);
        MoveRight.setTarget(this);
        MoveUp.setTarget(this);
        Scoop.setTarget(this);
        Sound.setTarget(this);
        Tick.setTarget(this);
        Heal.setTarget(this);
    }

    //Singleton
    public static GameWorld getInstance() {
        if (theGameWorldInstance == null)
            theGameWorldInstance = new GameWorld();
        return theGameWorldInstance;
    }

    /**
     * Creates the initial state of the GameWorld
     */
    public void initLayout() {
        //Ask for number of dogs
        dogsRemaining = Integer.parseInt(promptDogInput(false));
        catsRemaining = Integer.parseInt(promptCatInput(false));

        //the net will be in index 0
        theNet = new Net();
        //Wait until the object could be initialized / recreate if not initialized
        int count = 0;
        int maxTries = 16;
        boolean trying = true;
        while(trying) {
            try {
                theNet.toString();
                trying = false;
            } catch (Exception e) {
                if(count == maxTries){
                    e.printStackTrace();
                }
                theNet = new Net();
                System.out.println("net!");
                count++;
            }
        }
        gameObjects.add(theNet);

        //create cats
        for (int i = 0; i < catsRemaining; i++) {
            Cat aCat = new Cat();
            //Wait until the object could be initialized / recreate if not initialized
            count = 0;
            maxTries = 16;
            trying = true;
            while(trying) {
                try {
                    aCat.toString();
                    trying = false;
                } catch (Exception e) {
                    if(count == maxTries){
                        e.printStackTrace();
                    }
                    aCat = new Cat();
                    System.out.println("cat! " + i);
                    count++;

                }
            }
            gameObjects.add(aCat);
        }

        //create dogs
        for (int i = 0; i < dogsRemaining; i++) {
            Dog aDog = new Dog();
            //Wait until the object could be initialized / recreate if not initialized
            count = 0;
            maxTries = 16;
            trying = true;
            while(trying) {
                try {
                    aDog.toString();
                    trying = false;
                } catch (Exception e) {
                    if(count == maxTries){
                        e.printStackTrace();
                    }
                    aDog = new Dog();
                    System.out.println("dog! " + i);
                    count++;
                }
            }
            gameObjects.add(aDog);
        }

        promptForStart();
    }

    /**
     * Used to notify observers
     */
    public void modelUpdated() {
        setChanged();
        notifyObservers();
    }

    /**
     * Used to expand the net
     */
    public void expandNet() {
        theNet.expand();
    }

    /**
     * Used to contract the net
     */
    public void contractNet() {
        theNet.contract();
    }

    /**
     * Used to scoop up any animals on the net
     * Has rudimentary collision detection, (will need to change later if amount of objects increases.)
     * Detect collision by an intersection of two squares.
     */
    public void scoopAnimals() {
        //create shockwave
        gameObjects.add(new Shockwave((float)theNet.getLocation().getX(), (float)theNet.getLocation().getY()));

        Iterator<GameObject> it = gameObjects.iterator();
        while (it.hasNext()) {
            GameObject gObject = it.next();
            if (gObject instanceof ICollider) {
                ICollider cObject = (ICollider) gObject;
                if (theNet.collidesWith(cObject)) {
                    theNet.handleCollision(cObject);
                    if (cObject instanceof Dog) {
                        Dog theDog = (Dog) cObject;
                        totalPoints += 10 - theDog.getScratches();
                        cObject = null;
                        dogsCaptured++;
                        dogsRemaining--;
                        it.remove(); //Remove dog
                    } else if (cObject instanceof Cat) {
                        totalPoints -= 10;
                        cObject = null;
                        catsCaptured++;
                        catsRemaining--;
                        it.remove(); //Remove cat
                    }

                }

            }
        }
        if (dogsRemaining == 0) {
            endGame();
        }
    }

    /**
     * Used to move guidable objects to the right
     * Currently only the net
     */
    public void moveRight() {
        theNet.moveRight();
    }

    /**
     * Used to move guidable objects to the left
     * Currently only the net
     */
    public void moveLeft() {
        theNet.moveLeft();
    }

    /**
     * Used to move guidable objects up
     * Currently only the net
     */
    public void moveUp() {
        theNet.moveUp();
    }

    /**
     * Used to move guidable objects down
     * Currently only the net
     */
    public void moveDown() {
        theNet.moveDown();
    }

    /**
     * Tells movable objects to move (per tick)
     */
    public void tick() {
        for (IMovable movableObject : getMovableObjects())
            movableObject.move((int)((System.nanoTime() - startTime)/1000000));
        modelUpdated();
        startTime = System.nanoTime();

        //Start collision detection loop (after paint)
        Vector<ICollider> col = new Vector<ICollider>();
        //loop through game objects and make collection for ICollider items and ignore Net
        for (Object object : getGameObjects()) {
            if (object instanceof ICollider && !(object instanceof Net))
                col.add((ICollider) object);
        }
        //check collision detection between increasing pairs.
        //  (0,1)   (0,2)   (0,3)...(0,n)
        //          (1,2)   (1,3)...(1,n)
        //                  (2,3)...(2,n)
        //  .............................
        //                          (n-1,n)
        for (int i = 0; i < col.size(); i++) {
            for (int j = i + 1; j < col.size(); j++) {
                ICollider cObj1 = col.get(i);
                ICollider cObj2 = col.get(j);
                //do  not check collision between two same objects
                //shouldn't occur anyways
                if(cObj1 == cObj2)
                    continue;
                //if both object to be check are dogs, ignore, since collision between dogs is nonexistent
                if(cObj1 instanceof Dog && cObj2 instanceof Dog)
                    continue;
                //dog doesn't has collision detection implemented :/
                if(cObj1 instanceof Dog) {
                    if(cObj2.collidesWith(cObj1)){
                        cObj2.handleCollision(cObj1);
                    }
                }else{
                    if(cObj1.collidesWith(cObj2)){
                        cObj1.handleCollision(cObj2);
                    }
                }
            }
        }
    }

    /**
     * Toggle the sound
     */
    public void toggleSound() {
        sound = !sound;
        if(!alreadyStarted) {
            SoundHandler.soundHandlerInstance().playBG();
            alreadyStarted = true;
        }
        modelUpdated();
    }

    /**
     * Gets all objects
     *
     * @return the GameObjectCollection
     */
    public GameObjectCollection getGameObjects() {
        return gameObjects;
    }

    /**
     * Gets all movable objects
     *
     * @return An arraylist of movable objects
     */
    private ArrayList<IMovable> getMovableObjects() {
        ArrayList<IMovable> tempArray = new ArrayList<IMovable>();
        for (Object gObject : gameObjects) {
            if (gObject instanceof IMovable)
                tempArray.add((IMovable) gObject);
        }
        return tempArray;
    }

    public void addKitten(Cat theCat) {
        catsRemaining++;
        gameObjects.add(theCat);
    }

    /**
     * Gets all dogs
     *
     * @return An arraylist of dogs
     */
    public ArrayList<Dog> getDogs() {
        ArrayList<Dog> tempArray = new ArrayList<Dog>();
        for (Object gObject : gameObjects) {
            if (gObject instanceof Dog)
                tempArray.add((Dog) gObject);
        }
        return tempArray;
    }

    /**
     * Gets all cats
     *
     * @return An arraylist of cats
     */
    public ArrayList<Cat> getCats() {
        ArrayList<Cat> tempArray = new ArrayList<Cat>();
        for (Object gObject : gameObjects) {
            if (gObject instanceof Cat)
                tempArray.add((Cat) gObject);
        }
        return tempArray;
    }

    /**
     * Getter for catsCaptured
     *
     * @return catsCaptured
     */
    public int getCatsCaptured() {
        return catsCaptured;
    }

    /**
     * Getter for dogsCaptured
     *
     * @return dogsCaptured
     */
    public int getDogsCaptured() {
        return dogsCaptured;
    }

    /**
     * Getter for dogsRemaining
     *
     * @return dogsRemaining
     */
    public int getDogsRemaining() {
        return dogsRemaining;
    }

    /**
     * Getter for catsRemaining
     *
     * @return catsRemaining
     */
    public int getCatsRemaining() {
        return catsRemaining;
    }

    /**
     * Getter for totalPoints
     *
     * @return totalPoints
     */
    public int getTotalPoints() {
        return totalPoints;
    }

    /**
     * Getter for sound status
     *
     * @return sound
     */
    public boolean getSound() {
        return sound;
    }

    /**
     * Getter for the Net instance
     *
     * @return theNet
     */
    public Net getTheNet() {
        return theNet;
    }

    /**
     * Pop-up for dog amount input
     * http://stackoverflow.com/a/5169667
     * @param isRetry true if the input was wrong
     * @return the validated string that can be converted to an integer
     */
    private String promptDogInput(boolean isRetry)
    {
        String inputValue;

        if (isRetry)
            inputValue = JOptionPane.showInputDialog("Enter the amount of dogs you would like to start the world with?\n" +
                    "<html><font color=#ff0000>Please make sure that the amount is greater than 0 and less than or equal to 40</font></html>");
        else
            inputValue = JOptionPane.showInputDialog("Enter the amount of dogs you would like to start the world with?");

        if (inputValue == null || inputValue.isEmpty() || !inputValue.matches("[1-9][0-9]*") || Integer.parseInt(inputValue) > 40)
            inputValue = promptDogInput(true);

        return inputValue;
    }

    /**
     * Pop-up for cat amount input
     * http://stackoverflow.com/a/5169667
     * @param isRetry true if the input was wrong
     * @return the validated string that can be converted to an integer
     */
    private String promptCatInput(boolean isRetry)
    {
        String inputValue;
        if (isRetry)
            inputValue = JOptionPane.showInputDialog("Enter the amount of cats you would like to start the world with?\n" +
                    "<html><font color=#ff0000>Please make sure that the amount is greater than 0 and less than or equal to 40</font></html>");
        else
            inputValue = JOptionPane.showInputDialog("Enter the amount of cats you would like to start the world with?");

        if (inputValue == null || inputValue.isEmpty() || !inputValue.matches("[1-9][0-9]*") || Integer.parseInt(inputValue) > 40)
            inputValue = promptCatInput(true);

        return inputValue;
    }

    /**
     * Once start button is clicked, game execution will start
     * http://stackoverflow.com/a/5169667
     */
    private void promptForStart(){
        Object[] options = { "START" };
        int result = JOptionPane.showOptionDialog(null, "Press START to start the game", "Starting game",
                JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                null, options, options[0]);
        if(result == JOptionPane.CLOSED_OPTION)
            promptForStart();
    }

    /**
     * Displays the congratulations text for the end of the game
     * and then exits the game after the dialog is closed
     */
    private void endGame(){
        modelUpdated(); //one last update
        Game.getInstance().stopAnimation();
        JOptionPane.showMessageDialog(null, "All dogs have been caught!\nYour score is: " + totalPoints,
                "Congrats", JOptionPane.PLAIN_MESSAGE);
        System.exit(0);
    }

    /**
     * Reset the start time so there isn't a big jump in the animation after the game is unpaused
     */
    public void resetStartTime(){
        startTime = System.nanoTime();
    }

    public void removeShockwave(Shockwave sw){
        //for (Object gObject : gameObjects) {
         //   if (gObject instanceof Shockwave) {
          //      if(gObject.equals(sw))
                    gameObjects.remove(sw);
         //   }
     //   }

    }
}