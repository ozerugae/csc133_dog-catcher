package a4;

/**
 * Starter class; creates a new Game Object
 *
 * @see Game
 */
public class Starter {

    public static void main(String[] args) {
        Game g = Game.getInstance();
    }
}
