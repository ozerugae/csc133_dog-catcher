package a4.commands;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the About command
 * Currently shows an about message on a popup window
 */
public class About extends AbstractAction {
    private static volatile About aboutCommandInstance = null;

    private About() {
        super("About");
    }

    /**
     * Singleton implementation
     *
     * @return About action instance
     */
    public static About getInstance() {
        if (aboutCommandInstance == null) {
            aboutCommandInstance = new About();
        }
        return aboutCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Creates a popup window with the about message
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(null, "Name: Edward Ozeruga\nCourse: CSc 133\nAssignment: 3\n" +
                "Date: November 17, 2015", "About", JOptionPane.PLAIN_MESSAGE);
    }


}
