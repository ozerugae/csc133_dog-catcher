package a4.commands;

import a4.GameWorld;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Contract command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class Contract extends AbstractAction {
    private static volatile Contract contractCommandInstance = null;
    private GameWorld theWorld;

    private Contract() {
        super("Shrink Net");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Contract action instance
     */
    public static Contract setTarget(GameWorld theWorld) {
        if (contractCommandInstance == null) {
            contractCommandInstance = new Contract();
            contractCommandInstance.theWorld = theWorld;
        }
        return contractCommandInstance;
    }

    /**
     * //Singleton implementation
     *
     * @return Contract action instance
     */
    public static Contract getInstance() {
        if (contractCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return contractCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the GameWorld method: contractNet
     *
     * @param e
     * @see GameWorld#contractNet()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        theWorld.contractNet();
    }


}
