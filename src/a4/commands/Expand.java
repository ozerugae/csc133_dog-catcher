package a4.commands;

import a4.GameWorld;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Expand command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class Expand extends AbstractAction {
    private static volatile Expand expandCommandInstance = null;
    private GameWorld theWorld;

    private Expand() {
        super("Expand Net");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Expand action instance
     */
    public static Expand setTarget(GameWorld theWorld) {
        if (expandCommandInstance == null) {
            expandCommandInstance = new Expand();
            expandCommandInstance.theWorld = theWorld;
        }
        return expandCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Expand action instance
     */
    public static Expand getInstance() {
        if (expandCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return expandCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the GameWorld method: expandNet
     *
     * @param e
     * @see GameWorld#expandNet()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        theWorld.expandNet();
    }


}
