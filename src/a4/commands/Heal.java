package a4.commands;

import a4.Game;
import a4.GameWorld;
import a4.gameobjects.ISelectable;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Heal command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class Heal extends AbstractAction {
    private static volatile Heal healCommandInstance = null;
    private GameWorld theWorld;

    private Heal() {
        super("Heal");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Heal action instance
     */
    public static Heal setTarget(GameWorld theWorld) {
        if (healCommandInstance == null) {
            healCommandInstance = new Heal();
            healCommandInstance.theWorld = theWorld;
        }
        return healCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Heal action instance
     */
    public static Heal getInstance() {
        if (healCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return healCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * heals all selected items
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        for (Object o : theWorld.getGameObjects()) {
            if (o instanceof ISelectable) {
                if(((ISelectable)o).isSelected())
                    ((ISelectable)o).heal();
            }
        }
        Game.getInstance().repaintMap();
    }

    /**
     * disable/enable the button
     * @param bool
     */
    public void setEnabled(boolean bool){
        super.setEnabled(bool);
    }
}
