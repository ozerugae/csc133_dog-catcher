package a4.commands;

import a4.GameWorld;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Move Down command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class MoveDown extends AbstractAction {
    private static volatile MoveDown moveDownCommandInstance = null;
    private GameWorld theWorld;

    private MoveDown() {
        super("Move Down");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Move Down action instance
     */
    public static MoveDown setTarget(GameWorld theWorld) {
        if (moveDownCommandInstance == null) {
            moveDownCommandInstance = new MoveDown();
            moveDownCommandInstance.theWorld = theWorld;
        }
        return moveDownCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Move Down action instance
     */
    public static MoveDown getInstance() {
        if (moveDownCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return moveDownCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the GameWorld method: moveDown
     *
     * @param e
     * @see GameWorld#moveDown()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        theWorld.moveDown();
    }


}
