package a4.commands;

import a4.GameWorld;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Move Left command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class MoveLeft extends AbstractAction {
    private static volatile MoveLeft moveLeftCommandInstance = null;
    private GameWorld theWorld;

    private MoveLeft() {
        super("Move Left");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Move Left action instance
     */
    public static MoveLeft setTarget(GameWorld theWorld) {
        if (moveLeftCommandInstance == null) {
            moveLeftCommandInstance = new MoveLeft();
            moveLeftCommandInstance.theWorld = theWorld;
        }
        return moveLeftCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Move Left action instance
     */
    public static MoveLeft getInstance() {
        if (moveLeftCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return moveLeftCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the GameWorld method: moveLeft
     *
     * @param e
     * @see GameWorld#moveLeft()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        theWorld.moveLeft();
    }


}
