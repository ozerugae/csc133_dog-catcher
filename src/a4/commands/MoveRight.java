package a4.commands;

import a4.GameWorld;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Move Right command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class MoveRight extends AbstractAction {
    private static volatile MoveRight moveRightCommandInstance = null;
    private GameWorld theWorld;

    private MoveRight() {
        super("Move Right");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Move Right action instance
     */
    public static MoveRight setTarget(GameWorld theWorld) {
        if (moveRightCommandInstance == null) {
            moveRightCommandInstance = new MoveRight();
            moveRightCommandInstance.theWorld = theWorld;
        }
        return moveRightCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Move Right action instance
     */
    public static MoveRight getInstance() {
        if (moveRightCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return moveRightCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the GameWorld method: moveRight
     *
     * @param e
     * @see GameWorld#moveRight()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        theWorld.moveRight();
    }


}
