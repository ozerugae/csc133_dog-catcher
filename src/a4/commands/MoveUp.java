package a4.commands;

import a4.GameWorld;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Move Up command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class MoveUp extends AbstractAction {
    private static volatile MoveUp moveUpCommandInstance = null;
    private GameWorld theWorld;

    private MoveUp() {
        super("Move Up");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Move Up action instance
     */
    public static MoveUp setTarget(GameWorld theWorld) {
        if (moveUpCommandInstance == null) {
            moveUpCommandInstance = new MoveUp();
            moveUpCommandInstance.theWorld = theWorld;
        }
        return moveUpCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Move Up action instance
     */
    public static MoveUp getInstance() {
        if (moveUpCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return moveUpCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the GameWorld method: moveUp
     *
     * @param e
     * @see GameWorld#moveUp()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        theWorld.moveUp();
    }


}
