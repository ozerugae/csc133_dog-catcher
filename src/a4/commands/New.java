package a4.commands;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the New command
 * Currently does nothing
 */
public class New extends AbstractAction {
    private static volatile New newCommandInstance = null;

    private New() {
        super("New");
    }

    /**
     * Singleton implementation
     *
     * @return New action instance
     */
    public static New getInstance() {
        if (newCommandInstance == null) {
            newCommandInstance = new New();
        }
        return newCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Currently just prints out that action was invoked
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("New action invoked from " + e.getActionCommand() +
                " " + e.getSource().getClass());
    }


}
