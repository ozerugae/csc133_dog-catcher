package a4.commands;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/**
 * Action for the Quit command
 */
public class Quit extends AbstractAction {
    private static volatile Quit quitCommandInstance = null;

    private Quit() {
        super("Quit");
        putValue(MNEMONIC_KEY, KeyEvent.VK_Q);
    }

    /**
     * Singleton implementation
     *
     * @return Quit action instance
     */
    public static Quit getInstance() {
        if (quitCommandInstance == null) {
            quitCommandInstance = new Quit();
        }
        return quitCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Creates a confirmation popup. If user clicks yes, game will exit
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (JOptionPane.showConfirmDialog(null, "Are you sure you would like to quit?",
                "Quit?", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
            System.exit(0);
    }


}
