package a4.commands;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Save command
 * Currently does nothing
 */
public class Save extends AbstractAction {
    private static volatile Save saveCommandInstance = null;

    private Save() {
        super("Save");
    }

    /**
     * Singleton implementation
     *
     * @return Save action instance
     */
    public static Save getInstance() {
        if (saveCommandInstance == null) {
            saveCommandInstance = new Save();
        }
        return saveCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Currently just prints out that action was invoked
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Save action invoked from " + e.getActionCommand() +
                " " + e.getSource().getClass());
    }


}
