package a4.commands;

import a4.GameWorld;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Scoop command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class Scoop extends AbstractAction {
    private static volatile Scoop scoopCommandInstance = null;
    private GameWorld theWorld;

    private Scoop() {
        super("Scoop");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Scoop action instance
     */
    public static Scoop setTarget(GameWorld theWorld) {
        if (scoopCommandInstance == null) {
            scoopCommandInstance = new Scoop();
            scoopCommandInstance.theWorld = theWorld;
        }
        return scoopCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Scoop action instance
     */
    public static Scoop getInstance() {
        if (scoopCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return scoopCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the GameWorld method: scoopAnimals
     *
     * @param e
     * @see GameWorld#scoopAnimals()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        theWorld.scoopAnimals();
    }


}
