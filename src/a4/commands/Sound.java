package a4.commands;

import a4.GameWorld;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Sound command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class Sound extends AbstractAction {
    private static volatile Sound soundCommandInstance = null;
    private GameWorld theWorld;

    private Sound() {
        super("Sound");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Sound action instance
     */
    public static Sound setTarget(GameWorld theWorld) {
        if (soundCommandInstance == null) {
            soundCommandInstance = new Sound();
            soundCommandInstance.theWorld = theWorld;
        }
        return soundCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Sound action instance
     */
    public static Sound getInstance() {
        if (soundCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return soundCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the GameWorld method: toggleSound
     *
     * @param e
     * @see GameWorld#toggleSound()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        theWorld.toggleSound();
    }


}
