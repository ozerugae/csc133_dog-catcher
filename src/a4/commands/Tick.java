package a4.commands;

import a4.GameWorld;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Tick command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(GameWorld)
 */
public class Tick extends AbstractAction {
    private static volatile Tick tickCommandInstance = null;
    private GameWorld theWorld;

    private Tick() {
        super("Tick");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theWorld
     * @return Tick action instance
     */
    public static Tick setTarget(GameWorld theWorld) {
        if (tickCommandInstance == null) {
            tickCommandInstance = new Tick();
            tickCommandInstance.theWorld = theWorld;
        }
        return tickCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Tick action instance
     */
    public static Tick getInstance() {
        if (tickCommandInstance == null) {
            throw new IllegalStateException("Need to have the gameworld reference set first");
        }
        return tickCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the GameWorld method: tick
     *
     * @param e
     * @see GameWorld#tick()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        theWorld.tick();
    }


}
