package a4.commands;

import a4.Game;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Play command
 * <p>
 * Needs a reference to the GameWorld, use setTarget
 *
 * @see #setTarget(Game)
 */
public class ToggleGameState extends AbstractAction {
    private static volatile ToggleGameState toggleGameStateCommandInstance = null;
    private Game theGame;

    private ToggleGameState() {
        super("Pause");
    }

    /**
     * Singleton implementation with a requirement for a reference to GameWorld
     *
     * @param theGame
     * @return Play action instance
     */
    public static ToggleGameState setTarget(Game theGame) {
        if (toggleGameStateCommandInstance == null) {
            toggleGameStateCommandInstance = new ToggleGameState();
            toggleGameStateCommandInstance.theGame = theGame;
        }
        return toggleGameStateCommandInstance;
    }

    /**
     * Singleton implementation
     *
     * @return Play action instance
     */
    public static ToggleGameState getInstance() {
        if (toggleGameStateCommandInstance == null) {
            throw new IllegalStateException("Need to have the game reference set first");
        }
        return toggleGameStateCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Calls the Game method: resumeAnimation or
     * Calls the Game method: stopAnimation
     *
     * @param e
     * @see Game#resumeAnimation()
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Game theGame = Game.getInstance();
        if (theGame.isPaused()) {
            theGame.resumeAnimation();
            this.putValue(Action.NAME, "Pause");
        } else {
            theGame.stopAnimation();
            this.putValue(Action.NAME, "Play");
        }
    }

}
