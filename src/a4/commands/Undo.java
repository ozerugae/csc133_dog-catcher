package a4.commands;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Action for the Undo command
 * Currently does nothing
 */
public class Undo extends AbstractAction {
    private static volatile Undo undoCommandInstance = null;

    private Undo() {
        super("Undo");
    }

    /**
     * Singleton implementation
     *
     * @return Undo action instance
     */
    public static Undo getInstance() {
        if (undoCommandInstance == null) {
            undoCommandInstance = new Undo();
        }
        return undoCommandInstance;
    }

    /**
     * Invoked when an action occurs.
     * Currently just prints out that action was invoked
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Undo action invoked from " + e.getActionCommand() +
                " " + e.getSource().getClass());
    }


}
