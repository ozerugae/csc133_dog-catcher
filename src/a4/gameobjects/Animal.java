package a4.gameobjects;

import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Random;

/**
 * Abstract animal class; an animal that can be caught in the game
 */
public abstract class Animal extends GameObject implements IMovable {
    private int speed = 5;
    private int direction;

    /**
     * Animal's implementation of move. Moves an animal a certain speed in a certain direction per tick.
     * Should only be called once per tick.
     */
    public void move(int time) {
        if(this instanceof Dog)
            ((Dog)this).update();
        Random randomGen = new Random();
        Point2D.Float newLocation;
        if(randomGen.nextInt(100) > 50) {
            int dir;
            if (randomGen.nextInt(100) > 50) {
                dir = -5;
            } else{
                dir = 5;
            }
            setDirection(Math.abs((getDirection() + dir) % 360)); //-5 - 5 degrees
        }
        //setDirection(0);
        newLocation = new Point2D.Float((float) (getLocation().x + Math.cos(Math.toRadians(90 - direction)) * speed * time/125),
                (float) (getLocation().y + Math.sin(Math.toRadians(90 - direction)) * speed * time/125));
        setLocation(newLocation);
    }


    /**
     * Gets the direction of the animal
     *
     * @return the direction of the animal
     */
    public int getDirection() {
        return direction;
    }

    /**
     * TSets a new direction for the animal
     *
     * @param newDirection bounded between 0 nad 359 inclusive
     */
    protected void setDirection(int newDirection) {
        if (newDirection < 0 || newDirection > 359)
            throw new IllegalArgumentException("Illegal direction: 0 > " + newDirection + " > 359");
        direction = newDirection;
        rotate(newDirection);
    }

    /**
     * Gets the speed of the
     * animal
     *
     * @return the speed of the animal
     */
    protected int getSpeed() {
        return speed;
    }

    /**
     * Sets a new speed for the animal
     *
     * @param newSpeed bounded between 0 and 5 inclusive
     */
    protected void setSpeed(int newSpeed) {
        if (newSpeed < 0 || newSpeed > 5)
            throw new IllegalArgumentException("Illegal speed: 0 > " + newSpeed + " > 5");
        speed = newSpeed;
    }
}
