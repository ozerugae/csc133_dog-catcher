package a4.gameobjects;

import a4.GameWorld;
import a4.sound.SoundHandler;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Random;

/**
 * Animal - cat; can be scratches dogs
 */
public class Cat extends Animal implements IDrawable, ICollider {

    /**
     * Set up the initial conditions/status of the Cat
     */
    private int[] vx = new int[3];
    private int[] vy = new int[3];
    private boolean isKitten = true;
    private int kittenTreshold = 500;
    private int kittenWait = 0;
    private boolean canMakeKitten = true;
    private int makeKittenThreshold = 500;
    private int makeKittenWait = 0;
    private boolean canAttack = true;
    private int attackCooldown = 25;
    private int attackWait = 0;

    public Cat() {
        vx[0] = -1;
        vx[1] = 0;
        vx[2] = 1;

        vy[0] = -1;
        vy[1] = 1;
        vy[2] = -1;


        //set a random size, location, and direction
        Random randomGen = new Random();
        setSize((randomGen.nextInt(11) + 10)); //10-20 is reasonable-ish
        float randX = randomGen.nextFloat() * (GameWorld.width - getSize()) + getSize() / 2,
                randY = randomGen.nextFloat() * (GameWorld.height - getSize()) + getSize() / 2;
        setLocation(new Point2D.Float(randX, randY));
        setDirection(randomGen.nextInt(360));
        setColor(new java.awt.Color(35, 15, 0));
    }

    /**
     * Create a cat at a specified location
     * @param x - x coord
     * @param y - y coord
     */
    public Cat(float x, float y) {
        //set a random size and direction
        Random randomGen = new Random();
        setSize(randomGen.nextInt(11) + 10); //10-20 is reasonable-ish
        setLocation(new Point2D.Float(x, y));
        setDirection(randomGen.nextInt(360));
        setColor(new java.awt.Color(35, 15, 0));
    }

    /**
     * Called after a collision between two cats
     */
    public void makeKitten() {
        Cat aCat = new Cat(getLocation().x, getLocation().y);
        //Wait until the object could be initialized / recreate if not initialized
        int count = 0;
        int maxTries = 16;
        boolean trying = true;
        while(trying) {
            try {
                aCat.toString();
                trying = false;
            } catch (Exception e) {
                if(count == maxTries){
                    e.printStackTrace();
                }
                aCat = new Cat(getLocation().x, getLocation().y);
                System.out.println("kitten! ");
                count++;

            }
        }

        GameWorld.getInstance().addKitten(aCat);   //tell the gameworld to add the new kitten
    }

    @Override
    /**
     * Sets the color of the cat. Should only be called when a cat's color has not been set yet.
     * If called to change a cat's color, an exception will be thrown
     */
    protected final void setColor(Color newColor) {
        if (getColor() == null)
            super.setColor(new java.awt.Color(35, 15, 0));
        else //Do not change color, throw an exception if something tries to
            throw new IllegalStateException("Cannot change a cat's color");
    }

    @Override
    /**
     * Print out details of the cat for the map
     */
    public String toString() {
        return "Cat: loc=" + String.format("%.1f", getLocation().x) + "," + String.format("%.1f", getLocation().y) +
                " \tcolor=[" + getColor().getRed() + "," + getColor().getGreen() + "," + getColor().getBlue() +
                "] \tsize=" + getSize() + " \tspeed=" + getSpeed() + " \tdir=" + getDirection();
    }

    @Override
    /**
     * Draws the cat
     */
    public void draw(Graphics2D g) {
        if(isKitten) {
            if(kittenWait++ >= kittenTreshold)
                isKitten = false;
        }
        if(!canMakeKitten) {
            if(makeKittenWait++ >= makeKittenThreshold) {
                canMakeKitten = true;
                makeKittenWait = 0;
            }
        }
        if(!canAttack) {
            if(attackWait++ >= attackCooldown) {
                canAttack = true;
                attackWait = 0;
            }
        }
        //save the current graphics transform for later restoration
        AffineTransform saveAT = g.getTransform();

        g.setColor(getColor());

        //scale -> rotate -> translate
        g.transform(getTranslation());
        g.transform(getRotation());
        g.transform(getScale());

        g.fillPolygon(vx, vy, 3);

        //reset graphics transform
        g.setTransform(saveAT);
    }

    @Override
    /**
     * Checks for collision either with another cat or dog
     */
    public boolean collidesWith(ICollider otherObject) {
        /**
         * http://stackoverflow.com/posts/402010/revisions
         */
        float netX = getLocation().x + getSize();
        float netY = getLocation().y + getSize();
        float objX = 0;
        float objY = 0;
        int r = 0;

        //Another Cat
        if (otherObject instanceof Cat) {
            if(isKitten)
                return false;
            Cat theCat = (Cat) otherObject;
            r = theCat.getSize();
            objX = theCat.getLocation().x + r;
            objY = theCat.getLocation().y + r;
        }
        //A dog
        else if (otherObject instanceof Dog) {
            if(!canAttack)
                return false;
            Dog theDog = (Dog) otherObject;
            r = theDog.getSize();
            objX = theDog.getLocation().x + r;
            objY = theDog.getLocation().y + r;
        }

        int distX = (int) Math.abs(objX - netX);
        int distY = (int) Math.abs(objY - netY);

        if (distX > (getSize() + r)) { return false; }
        if (distY > (getSize()  + r)) { return false; }

        if (distX <= (getSize() )) { return true; }
        if (distY <= (getSize() )) { return true; }

        int corner = (distX - getSize())^2 +
                (distY - getSize())^2;

        return (corner <= (r^2));
    }


    @Override
    /**
     * Handles a collision between two cats or a cat and a dog
     */
    public void handleCollision(ICollider otherObject) {
        //Another Cat
        if(otherObject instanceof Cat){
            //Can only make a cat if this and the other cat can
            if(canMakeKitten() && ((Cat) otherObject).canMakeKitten()) {
                makeKitten();
                ((Cat) otherObject).toggleMakeKitten();
                toggleMakeKitten();
                SoundHandler.soundHandlerInstance().playCat();
            }
        }
        //A dog
        else if(otherObject instanceof Dog){
            //Can only attack again after 10 game ticks
            if(canAttack) {
                ((Dog) otherObject).scratched();
                canAttack = false;
            }
        }
    }

    /**
     * Check if the cat can make a kitten
     * Based on three rules: cannot be a kitten, cannot have made a kitten recently, and cannot be a 201st kitten
     * @return true if can make a kitten
     */
    public boolean canMakeKitten(){
        return (!isKitten && canMakeKitten && GameWorld.getInstance().getCatsRemaining() < 40);
    }

    /**
     * Set canMakeKitten status to false
     */
    public void toggleMakeKitten(){
        canMakeKitten = false;
    }
}
