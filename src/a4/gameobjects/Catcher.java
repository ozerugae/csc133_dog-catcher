package a4.gameobjects;

/**
 * Abstract catcher class; guided by a player and used to catch animals
 */
public abstract class Catcher extends GameObject implements IGuidable {
    public void moveUp() {
    }

    public void moveDown() {
    }

    public void moveLeft() {
    }

    public void moveRight() {
    }
}
