package a4.gameobjects;

import a4.Game;
import a4.GameWorld;
import a4.gameobjects.dogcomponents.Body;
import a4.gameobjects.dogcomponents.Feet;
import a4.gameobjects.dogcomponents.Head;
import a4.sound.SoundHandler;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Random;

/**
 * Animal - dog; can be scratched by cats
 */
public class Dog extends Animal implements IDrawable, ICollider, ISelectable{
    private int scratches = 0;
    boolean selected = false;

    private Body theBody;
    private Head theHead;
    private Feet[] theFeet = new Feet[4];

    private double feetOffset = 0 ; //current feet distance from body
    private double feetIncrement = +0.05 ; // change in feet distance each tick
    private double maxFeetOffset = 1.5;

    /**
     * Set up the initial conditions/status of the Dog
     */
    public Dog() {
        //set a random size, location, and direction
        Random randomGen = new Random();
        setSize(randomGen.nextInt(16) + 20); //20 - 35 is reasonable-ish
        float randX = randomGen.nextFloat() * (GameWorld.width - getSize()) + getSize() / 2,
                randY = randomGen.nextFloat() * (GameWorld.height - getSize()) + getSize() / 2;
        setLocation(new Point2D.Float(randX, randY));
        setDirection(randomGen.nextInt(360));
        setColor(new java.awt.Color(255, 195, 85));

        theBody = new Body(this);
        theHead = new Head(this);
        theFeet[0] = new Feet(this);
        theFeet[1] = new Feet(this);
        theFeet[2] = new Feet(this);
        theFeet[3] = new Feet(this);

    }

    /**
     * Called after a collision between a cat and a dog
     */
    public void scratched() {
        if (scratches >= 5)
            return;
        SoundHandler.soundHandlerInstance().playDog();
        scratches++;
        //subtract green and blue by 20 each
        setColor(new java.awt.Color(255, Math.max(0, getColor().getGreen() - 20),
                Math.max(0, getColor().getBlue() - 20)));
        setSpeed(getSpeed() - 1);
    }

    /**
     * Gets the amount of scratches the dog suffered
     *
     * @return the number of scratches
     */
    public int getScratches() {
        return scratches;
    }

    public void update () {
        // update the feet positions (move them along their local Y axis)
        feetOffset += feetIncrement * (getSpeed()+1) ;
        theFeet[0].translate (1, 3 + feetOffset);
        theFeet[1].translate (-2, 3 - feetOffset);
        theFeet[2].translate (1 , -1 + feetOffset);
        theFeet[3].translate (-2, -1 - feetOffset);
        // reverse direction of feet movement for next time if we've hit the max
        if (Math.abs(feetOffset) >= maxFeetOffset) {
            feetIncrement *= -1 ;
        }
    }

    @Override
    /**
     * Print out details of the dog for the map
     */
    public String toString() {
        return "Dog: loc=" + String.format("%.1f", getLocation().x) + "," + String.format("%.1f", getLocation().y) +
                " \tcolor=[" + getColor().getRed() + "," + getColor().getGreen() + "," + getColor().getBlue() +
                "] \tsize=" + getSize() + " \tspeed=" + getSpeed() + " \tdir=" + getDirection() + "   \tscratches=" + scratches;
    }

    @Override
    public void draw(Graphics2D g) {
        if(!Game.getInstance().isPaused() && selected)
            selected = false;
        //save the current graphics transform
        AffineTransform saveAT = g.getTransform();

        g.setColor(getColor());

        //scale -> rotate -> translate
        g.transform(getTranslation());
        g.transform(getRotation());
        g.transform(getScale());



        theBody.draw(g);
        theHead.draw(g);
        for (Feet f : theFeet) {
            f.draw(g);
        }

        //reset graphics transform
        g.setTransform(saveAT);

        if(isSelected()) {
            g.transform(getTranslation());
            g.transform(getRotation());

            g.setColor(Color.BLUE);
            g.drawRect(-getSize(), -getSize(), getSize(), getSize()*2);

            //reset graphics transform
            g.setTransform(saveAT);
        }
    }


    @Override
    public boolean collidesWith(ICollider otherObject) {
       return false;
    }

    @Override
    public void handleCollision(ICollider otherObject) {
        
    }

    @Override
    public void setSelected(boolean flag) {
        if(Game.getInstance().isPaused()){
            selected = flag;
        }
    }

    @Override
    public boolean isSelected() {
        return selected ;
    }

    @Override
    public boolean contains(Point2D p) {
        if(theFeet[0].contains(p))
            return true;
        if(theFeet[1].contains(p))
            return true;
        if(theFeet[2].contains(p))
            return true;
        if(theFeet[3].contains(p))
            return true;
        if(theBody.contains(p))
            return true;
        if(theHead.contains(p))
            return true;

        int px = (int) p.getX(); // mouse location
        int py = (int) p.getY();
        int xLoc = (int)getLocation().getX(); // shape location
        int yLoc = (int)getLocation().getY();
        return (px >= xLoc - getSize()) && (px <= xLoc + getSize()) && (py >= yLoc - getSize() * 2) && (py <= yLoc + getSize() * 2);
    }

    @Override
    public void heal(){
        setColor(new java.awt.Color(255, Math.max(0, getColor().getGreen() + 20 * scratches),
                Math.max(0, getColor().getBlue() + 20 * scratches)));
        setSpeed(getSpeed() + 1 * scratches);
        scratches = 0;
    }
}
