package a4.gameobjects;

import a4.GameWorld;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 * The parent class of all objects that appear in the GameWorld
 */
public abstract class GameObject {

    private Point2D.Float location;
    private Color color;
    private int size;

    private AffineTransform myTranslation;
    private AffineTransform myRotation;
    private AffineTransform myScale;

    public GameObject(){
        //initialize the transformations
        myTranslation = new AffineTransform();
        myRotation = new AffineTransform();
        myScale = new AffineTransform();
    }

    /**
     * Used to get the location of the game object
     *
     * @return The object's (x,y) location
     */
    public Point2D.Float getLocation() {
        return new Point2D.Float((float)getTranslation().getTranslateX(), (float)getTranslation().getTranslateY());
    }

    /**
     * Used to set a new location for a game object.
     *
     * @param newLocation The new location's x coordinate
     */
    protected void setLocation(Point2D.Float newLocation) {
        translate(newLocation.getX(), newLocation.getY());
    }

    /**
     * Used to get the color of the game object
     *
     * @return The object's color
     */
    protected Color getColor() {
        return color;
    }

    /**
     * Used to set a new color for a game object.
     *
     * @param newColor The new color of the object
     */
    protected void setColor(Color newColor) {
        color = newColor;
    }

    /**
     * Used to get the size of the game object
     *
     * @return The object's size
     */
    public int getSize() {
        return (int) getScale().getScaleX();
    }

    /**
     * Used to set a new size for a game object.
     *
     * @param newSize The new size of the object
     */
    protected void setSize(float newSize) {
        size = (int)newSize;
        scale(newSize, newSize);
    }

    public void rotate(double degrees) {
        myRotation.setToIdentity();
        myRotation.rotate(Math.toRadians((-degrees)));
    }
    public void scale(double sx, double sy) {
        myScale.setToIdentity();
        myScale.scale(sx, sy);
    }
    public void translate(double dx, double dy) {
        myTranslation.setToIdentity();
        myTranslation.translate(dx, dy);
    }

    public AffineTransform getTranslation(){
        return myTranslation;
    }
    public AffineTransform getRotation(){
        return myRotation;
    }
    public AffineTransform getScale(){
        return myScale;
    }
}
