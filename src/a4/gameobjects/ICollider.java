package a4.gameobjects;

/**
 * Interface for game objects that handle collision
 */
public interface ICollider {
    /**
     * Check if collision occurs
     * @param otherObject the other object that needs to be checked against collision for
     * @return true if collision occurs
     */
    boolean collidesWith(ICollider otherObject);

    /**
     * Handle collision event
     * @param otherObject the object in collision with
     */
    void handleCollision(ICollider otherObject);
}
