package a4.gameobjects;

import java.awt.*;

/**
 * Interface for game objects that can be drawn
 */
public interface IDrawable {
    void draw(Graphics2D g);
}
