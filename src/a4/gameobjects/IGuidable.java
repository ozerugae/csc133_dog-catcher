package a4.gameobjects;

/**
 * Interface for game objects that can be guided by the player
 */
public interface IGuidable {
    void moveUp();

    void moveDown();

    void moveLeft();

    void moveRight();
}
