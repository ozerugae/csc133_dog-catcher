package a4.gameobjects;

/**
 * Interface for game objects that move by themselves
 */
public interface IMovable {
    void move(int time);
}
