package a4.gameobjects;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * Interface for game objects that can be selected during game pause
 */
public interface ISelectable {
    //mark an object as selected
    void setSelected(boolean flag);
    //test whether an object is selected
    boolean isSelected();
    //determine if a mouse point is in an object
    boolean contains(Point2D p);
    //heal
    void heal();
}
