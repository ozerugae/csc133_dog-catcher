package a4.gameobjects;

import a4.GameWorld;
import a4.sound.SoundHandler;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Random;

/**
 * Net class; guidable by a player, used to catch animals in the vicinity of the net
 */
public class Net extends Catcher implements IDrawable, ICollider{
    public Net() {
        //Set a size, color, and a random location
        Random randomGen = new Random();
        setSize(100);
        float randX = randomGen.nextFloat() * (GameWorld.width - getSize()) + getSize() / 2,
                randY = randomGen.nextFloat() * (GameWorld.height - getSize() + getSize() / 2);
        setLocation(new Point2D.Float(randX, randY));
        setColor(new java.awt.Color(50, 50, 50));
    }

    /**
     * Expands the net by 10
     */
    public void expand() {
        if (getSize() > 140)
            setSize(150);
        setSize(getSize() + 10);
    }

    /**
     * Contracts the net by 10
     */
    public void contract() {
        if (getSize() < 60)
            setSize(50);
        setSize(getSize() - 10);
    }

    @Override
    /**
     * Move the net down by 5
     */
    public void moveDown() {
        Point2D.Float newLocation = new Point2D.Float(getLocation().x, getLocation().y - 5);
        setLocation(newLocation);
    }

    @Override
    /**
     * Move the net up by 5
     */
    public void moveUp() {
        Point2D.Float newLocation = new Point2D.Float(getLocation().x, getLocation().y + 5);
        setLocation(newLocation);
    }

    @Override
    /**
     * Move the net left by 5
     */
    public void moveLeft() {
        Point2D.Float newLocation = new Point2D.Float(getLocation().x - 5, getLocation().y);
        setLocation(newLocation);
    }

    @Override
    /**
     * Move the net right by 5
     */
    public void moveRight() {
        Point2D.Float newLocation = new Point2D.Float(getLocation().x + 5, getLocation().y);
        setLocation(newLocation);
    }

    @Override
    /**
     * Print out details of the net for the map
     */
    public String toString() {
        return "Net: loc=" + String.format("%.1f", getLocation().x) + "," + String.format("%.1f", getLocation().y) +
                " \tcolor=[" + getColor().getRed() + "," + getColor().getGreen() + "," + getColor().getBlue() +
                "] \tsize=" + getSize();
    }

    @Override
    /**
     * Sets the color of the net. Should only be called when a net's color has not been set yet.
     * If called to change a net's color, an exception will be thrown
     */
    protected final void setColor(Color newColor) {
        if (getColor() == null)
            super.setColor(new java.awt.Color(50, 50, 50));
        else //Do not change color, throw an exception if something tries to
            throw new IllegalStateException("Cannot change a net's color");
    }

    @Override
    /**
     * Draws the net
     */
    public void draw(Graphics2D g) {
        //save the current graphics transform
        AffineTransform saveAT = g.getTransform();

        //scale -> translate
        g.transform(getTranslation());
        g.transform(getScale());


        g.setColor(getColor());
        g.fillRect(-1, -1, 2, 2);

        //reset graphics transform
        g.setTransform(saveAT);
    }

    @Override
    /**
     * Checks for collision between the net and cat or dog
     */
    public boolean collidesWith(ICollider otherObject) {
        /**
         * http://stackoverflow.com/posts/402010/revisions
         */
        float netX = getLocation().x + getSize()/2;
        float netY = getLocation().y + getSize()/2;
        float objX = 0;
        float objY = 0;
        int r = 0;

        //A Cat
        if(otherObject instanceof Cat){
            Cat theCat = (Cat)otherObject;
            r = theCat.getSize();
            objX = theCat.getLocation().x + r;
            objY = theCat.getLocation().y + r;
        }
        //A dog
        else if(otherObject instanceof Dog){
            Dog theDog = (Dog)otherObject;
            r = theDog.getSize();
            objX = theDog.getLocation().x + r;
            objY = theDog.getLocation().y + r;
        }

        int distX = (int)Math.abs(objX - netX);
        int distY = (int)Math.abs(objY - netY);

        if (distX > (getSize() + r)) { return false; }
        if (distY > (getSize() + r)) { return false; }

        if (distX <= (getSize())) { return true; }
        if (distY <= (getSize())) { return true; }

        int corner = (distX - getSize())^2 +
                (distY - getSize())^2;

        return (corner <= (r^2));
    }

    @Override
    /**
     * Handle the collision. Currently in the GameWorld
     */
    public void handleCollision(ICollider otherObject) {
        //Handled in GameWorld
        SoundHandler.soundHandlerInstance().playNet();
    }
}
