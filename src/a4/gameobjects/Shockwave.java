package a4.gameobjects;

import a4.GameWorld;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Random;
import java.util.Vector;

/**
 * Shockwave class, create shockwave objects doesn't look to round-ish
 */
public class Shockwave extends GameObject implements IMovable, IDrawable {

    private Graphics2D g;
    private Vector<Point2D> pV = new Vector<Point2D>();
    private int maxLevel = 12;
    private int direction;
    private int speed = 25;
    private int remainingLifetime = 7500;


    public Shockwave(float x, float y) {
        initPoints();
        translate(x, y);
        direction = randomInteger(0, 359);
        rotate(direction);
        scale(4,4);
    }

    @Override
    public void move(int time) {
        Point2D newLocation = new Point2D.Float((float) (getLocation().x + Math.cos(Math.toRadians(90 - direction)) * speed * time/125),
                (float) (getLocation().y + Math.sin(Math.toRadians(90 - direction)) * speed * time/125));
        translate(newLocation.getX(), newLocation.getY());
        remainingLifetime -= time;
        if(remainingLifetime <= 0){
            GameWorld.getInstance().removeShockwave(this);
        }
    }

    //initialize shockwave with 4 control points
    private void initPoints() {
        if (pV.isEmpty()) {
            pV.add(new Point(randomInteger(-20, -10), randomInteger(-20, -10)));
            pV.add(new Point(randomInteger(-15, -5), randomInteger(10, 20)));
            pV.add(new Point(randomInteger(5, 15), randomInteger(10, 20)));
            pV.add(new Point(randomInteger(10, 20), randomInteger(-20, -10)));
        }
        System.out.println(pV);
    }

    public void draw(Graphics2D g) {
        this.g = g;
        // save the AT for restoration later
        AffineTransform saveAT = g.getTransform();

        // add current objects transformations
        g.transform(getTranslation());
        g.transform(getRotation());
        g.transform(getScale());

        // set color and draw the bezier curve
        g.setColor(new Color(127,127,127));
        drawBezierCurve(pV, 0);

        // restore the g2d object for the next object
        g.setTransform(saveAT);
    }

    private void drawBezierCurve(Vector<Point2D> controls, int level) {
        if (straightEnough(controls) || (level > maxLevel))
            g.drawLine((int)(controls.get(0).getX()), (int)(controls.get(0).getY()), (int)(controls.get(3).getX()), (int)(controls.get(3).getY()));
        else{
            Vector<Point2D> left = new Vector<Point2D>(4), right = new Vector<Point2D>(4);
            //initialize empty so I can use set.
            left.add(null);
            left.add(null);
            left.add(null);
            left.add(null);
            right.add(null);
            right.add(null);
            right.add(null);
            right.add(null);
            subdivideCurve(controls, left, right);
            drawBezierCurve(left, level + 1);
            drawBezierCurve(right, level + 1);
        }
    }

    private void subdivideCurve(Vector<Point2D> controls, Vector<Point2D> l, Vector<Point2D> r) {
        //from lecture notes
        l.set(0, new Point((int) (controls.get(0).getX()), (int) (controls.get(0).getY())));
        l.set(1, new Point((int) ((controls.get(0).getX() + controls.get(1).getX()) / 2.0), (int) (((controls.get(0).getY() + controls.get(1).getY()) / 2.0))));
        l.set(2, new Point((int) ((l.get(1).getX() / 2.0) + (controls.get(1).getX() + controls.get(2).getX()) / 4.0), (int) ((l.get(1).getY() / 2.0) + (controls.get(1).getY() + controls.get(2).getY()) / 4.0)));
        r.set(3, new Point((int) (controls.get(3).getX()), (int) (controls.get(3).getY())));
        r.set(2, new Point((int) ((controls.get(2).getX() + controls.get(3).getX()) / 2.0), (int) ((controls.get(2).getY() + controls.get(3).getY()) / 2.0)));
        r.set(1, new Point((int) ((controls.get(1).getX() + controls.get(2).getX()) / 4.0 + r.get(2).getX() / 2.0), (int) ((controls.get(1).getY() + controls.get(2).getY()) / 4.0 + r.get(2).getY() / 2.0)));
        l.set(3, new Point((int) ((l.get(2).getX() + r.get(1).getX()) / 2.0), (int) ((l.get(2).getY() + r.get(1).getY()) / 2.0)));
        r.set(0, new Point((int) (l.get(3).getX()), (int) (l.get(3).getY())));
    }


    private boolean straightEnough(Vector<Point2D> test) {
        //lecture notes
        float d1, d2;
        d1 = (float)((test.get(0).distance(test.get(1))) + (test.get(1).distance(test.get(2))) + (test.get(2).distance(test.get(3))));
        d2 = (float)(test.get(0).distance(test.get(3)));

        return Math.abs(d1 - d2) < 0.001;
    }

    private int randomInteger(int min, int max) {
        Random rand = new Random();
        int randomNum = min + (int)(Math.random() * ((max - min) + 1));
        return randomNum;
    }

}
