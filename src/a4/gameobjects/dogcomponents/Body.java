package a4.gameobjects.dogcomponents;

import a4.gameobjects.Dog;

import java.awt.*;
import java.awt.geom.Point2D;

/**
 * Dog - body graphic part
 * An oval
 */
public class Body {
    private Dog theDog;

    public Body(Dog theDog){
        this.theDog = theDog;
    }

    public void draw(Graphics2D g){
        g.fillOval(-1, -1, 1, 2);
    }
    public boolean contains(Point2D p) {
        int px = (int) p.getX(); // mouse location
        int py = (int) p.getY();
        int xLoc = (int)(theDog.getLocation().getX()); // shape location
        int yLoc = (int)(theDog.getLocation().getY());
        return (px >= xLoc - 4) && (px <= xLoc + 4) && (py >= yLoc - 3) && (py <= yLoc + 3);
    }
}
