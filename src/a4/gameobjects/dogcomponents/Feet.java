package a4.gameobjects.dogcomponents;

import a4.gameobjects.Dog;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 * Dog - Feet graphic part
 * circles
 */
public class Feet{
    private AffineTransform t,s;
    private Dog theDog;

    public Feet(Dog theDog){
        t = new AffineTransform();
        s = new AffineTransform();
        this.theDog = theDog;
    }
    public void draw(Graphics2D g){
        AffineTransform saveAT = g.getTransform();
        scale(0.5, 0.5);
        g.transform(s);
        g.transform(t);
        g.fillOval(-1, -1, 1, 1);
        g.setTransform (saveAT);
    }
    public void scale(double sx, double sy) {
        s.setToIdentity();
        s.scale(sx, sy);
    }
    public void translate(double dx, double dy) {
        t.setToIdentity();
        t.translate(dx, dy);
    }

    public boolean contains(Point2D p) {
        int px = (int) p.getX(); // mouse location
        int py = (int) p.getY();
        int xLoc = (int)(theDog.getLocation().getX() + t.getTranslateX()*s.getScaleX()); // shape location
        int yLoc = (int)(theDog.getLocation().getY() + t.getTranslateY()*s.getScaleX());
        return (px >= xLoc - 3) && (px <= xLoc + 3) && (py >= yLoc - 3) && (py <= yLoc + 3);
    }
}

