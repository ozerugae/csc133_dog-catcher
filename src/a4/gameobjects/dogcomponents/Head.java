package a4.gameobjects.dogcomponents;

import a4.gameobjects.Dog;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

/**
 * Dog - Head graphic part
 * A triangle
 */
public class Head{
    private int[] vx = new int[3];
    private int[] vy = new int[3];
    private AffineTransform t,s;
    private Dog theDog;

    public Head(Dog theDog){
        vx[0] = -1;
        vx[1] = 0;
        vx[2] = 1;

        vy[0] = -1;
        vy[1] = 1;
        vy[2] = -1;
        t = new AffineTransform();
        s = new AffineTransform();
        this.theDog = theDog;
    }
    public void draw(Graphics2D g){
        AffineTransform saveAT = g.getTransform();
        translate(-1, 3);
        scale(0.5, 0.5);
        g.transform(s);
        g.transform(t);
        g.fillPolygon(vx, vy, 3);
        g.setTransform (saveAT);
    }
    public void scale(double sx, double sy) {
        s.setToIdentity();
        s.scale(sx, sy);
    }
    public void translate(double dx, double dy) {
        t.setToIdentity();
        t.translate(dx, dy);
    }
    public boolean contains(Point2D p) {
        int px = (int) p.getX(); // mouse location
        int py = (int) p.getY();
        int xLoc = (int)(theDog.getLocation().getX() + Math.cos(Math.toRadians(theDog.getDirection())) * t.getTranslateX()*s.getScaleX()); // shape location
        int yLoc = (int)(theDog.getLocation().getY() + Math.sin(Math.toRadians(theDog.getDirection())) * t.getTranslateY()*s.getScaleY());
        return (px >= xLoc - 3) && (px <= xLoc + 3) && (py >= yLoc - 3) && (py <= yLoc + 3);
    }
}
