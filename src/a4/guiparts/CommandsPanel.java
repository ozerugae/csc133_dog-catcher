package a4.guiparts;

import a4.commands.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * The CommandsPanel JPanel. Contains all of the buttons that
 * can be used to control the game
 */
public class CommandsPanel extends JPanel {
    private JButton expandNet, shrinkNet, scoop, moveRight, moveLeft,
            moveUp, moveDown, heal, toggleGameState;

    /**
     * Set up the commands view
     */
    public CommandsPanel() {
        setLayout(new GridLayout(10, 1, 5, 0));
        setBorder(new TitledBorder(BorderFactory.createLineBorder(new Color(64, 32, 128), 2, true),
                "Commands:"));                      //create a titled border
        //Initialize the buttons
        expandNet = new JButton("Expand Net");
        shrinkNet = new JButton("Shrink Net");
        scoop = new JButton("Scoop");
        moveRight = new JButton("Move Right");
        moveLeft = new JButton("Move Left");
        moveUp = new JButton("Move Up");
        moveDown = new JButton("Move Down");
        heal = new JButton("Heal");
        toggleGameState = new JButton();


        //Set an action to be called on button click
        expandNet.setAction(Expand.getInstance());
        shrinkNet.setAction(Contract.getInstance());
        scoop.setAction(Scoop.getInstance());
        moveRight.setAction(MoveRight.getInstance());
        moveLeft.setAction(MoveLeft.getInstance());
        moveUp.setAction(MoveUp.getInstance());
        moveDown.setAction(MoveDown.getInstance());
        heal.setAction(Heal.getInstance());
        toggleGameState.setAction(ToggleGameState.getInstance());

        //Add the buttons to the panel
        add(expandNet);
        add(shrinkNet);
        add(scoop);
        add(moveRight);
        add(moveLeft);
        add(moveUp);
        add(moveDown);
        add(heal);
        add(toggleGameState);
        Heal.getInstance().setEnabled(false);
    }

}
