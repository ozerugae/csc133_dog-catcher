package a4.guiparts;

import a4.Game;
import a4.GameWorld;
import a4.commands.*;
import a4.gameobjects.GameObject;
import a4.gameobjects.IDrawable;
import a4.gameobjects.ISelectable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Observable;
import java.util.Observer;

/**
 * The MapView JPanel. Updates the locations of the game's objects
 * when the model changes
 * Currently is a placeholder
 */
public class MapView extends JPanel implements Observer, MouseListener, MouseMotionListener, MouseWheelListener {
    private GameWorld theWorld;
    private Game theGame;
    private double windowLeft, windowRight, windowTop, windowBottom;
    private AffineTransform worldToND, ndToScreen, theVTM;
    private Point2D prevMousePosition;


    /**
     * Set up the view
     */
    public MapView(Game theGame) {
        setLayout(new GridBagLayout());

        addMouseListener(this);
        addMouseWheelListener(this);
        addMouseMotionListener(this);

        windowLeft = 0;
        windowRight = 1220;
        windowTop = 962;
        windowBottom = 0;

        //Keybinding for the relevant buttons
        InputMap iM = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        ActionMap aM = getActionMap();
        iM.put(KeyStroke.getKeyStroke("S"), "scoop");
        aM.put("scoop", Scoop.getInstance());
        iM.put(KeyStroke.getKeyStroke("RIGHT"), "right");
        aM.put("right", MoveRight.getInstance());
        iM.put(KeyStroke.getKeyStroke("LEFT"), "left");
        aM.put("left", MoveLeft.getInstance());
        iM.put(KeyStroke.getKeyStroke("UP"), "up");
        aM.put("up", MoveUp.getInstance());
        iM.put(KeyStroke.getKeyStroke("DOWN"), "down");
        aM.put("down", MoveDown.getInstance());
        iM.put(KeyStroke.getKeyStroke("Q"), "quit");
        aM.put("quit", Quit.getInstance());

        this.theGame = theGame;
    }

    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     *
     * @param o   the observable object.
     * @param arg an argument passed to the <code>notifyObservers</code>
     */
    @Override
    public void update(Observable o, Object arg) {
        if (theWorld == null)
            theWorld = (GameWorld) o;
        repaint();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        //save the current graphics transform
        AffineTransform saveAT = g2d.getTransform();

        //update the Viewing Transformation Matrix
        //System.out.println((windowRight - windowLeft)/(windowTop - windowBottom));
        worldToND = buildWorldToNDXform(windowRight, windowTop, windowLeft, windowBottom);
        ndToScreen = buildNDToScreenXform(this.getWidth(), this.getHeight());
        theVTM = (AffineTransform) ndToScreen.clone();
        theVTM.concatenate(worldToND); // matrix mult � note order!
        //concatenate the VTM onto the g2d�s current transformation
        g2d.transform(theVTM);

        //Draw background
        g2d.setColor(Color.LIGHT_GRAY);
        g2d.fillRect((int) windowLeft, (int) windowBottom, (int) windowRight - (int) windowLeft, (int) windowTop - (int) windowBottom);

        for (Object o : theWorld.getGameObjects()) {
            GameObject gameObject = (GameObject) o;
            IDrawable drawable = (IDrawable) gameObject;
            drawable.draw(g2d);
        }

        //reset graphics transform
        g2d.setTransform(saveAT);
    }

    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     *
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {

    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     *
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {

        Point p = e.getPoint();
        AffineTransform inverse;
        try {
            inverse = theVTM.createInverse();
        } catch (Exception ex) {
            System.out.println("Non-Invertible: " + ex.toString());
            return;
        }
        Point2D p2 = inverse.transform(p, null);
        prevMousePosition = inverse.transform(e.getPoint(), null);
        if (e.isControlDown()) {
            for (Object o : theWorld.getGameObjects()) {
                if (o instanceof ISelectable)
                    if (((ISelectable) o).contains(p2)) {
                        ((ISelectable) o).setSelected(true);
                    }
            }
        } else {
            for (Object o : theWorld.getGameObjects()) {
                if (o instanceof ISelectable)
                    ((ISelectable) o).setSelected(((ISelectable) o).contains(p2));
            }
        }
        repaint();
    }

    /**
     * Invoked when a mouse button has been released on a component.
     *
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * Invoked when the mouse enters a component.
     *
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * Invoked when the mouse exits a component.
     *
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {

    }

    /**
     * World to ND
     *
     * @param winR right
     * @param winT top
     * @param winL left
     * @param winB bottom
     * @return
     */
    private AffineTransform buildWorldToNDXform(double winR, double winT, double winL, double winB) {
        AffineTransform newAT = new AffineTransform();
        double h = winT - winB;
        double w = winR - winL;
        newAT.scale(1 / w, 1 / h);
        newAT.translate(-winL, -winB);
        return newAT;
    }

    /**
     * ND to screen
     *
     * @param width  width
     * @param height height
     * @return
     */
    private AffineTransform buildNDToScreenXform(int width, int height) {
        AffineTransform newAT = new AffineTransform();
        newAT.translate(0, height);
        newAT.scale(width, -height);
        return newAT;
    }

    /**
     * Zoom in window
     */
    private void zoomIn() {
        double h = windowTop - windowBottom;
        double w = windowRight - windowLeft;
        windowLeft += w * 0.05;
        windowRight -= w * 0.05;
        windowTop -= h * 0.05;
        windowBottom += h * 0.05;
        this.repaint();
    }

    /**
     * Zoom out window
     */
    private void zoomOut() {
        double h = windowTop - windowBottom;
        double w = windowRight - windowLeft;

        windowLeft -= w * 0.05;
        windowRight += w * 0.05;
        windowTop += h * 0.05;
        windowBottom -= h * 0.05;

        this.repaint();
    }

    /**
     * Pan window right
     */
    private void panRight() {
        windowLeft += 5 * (windowRight - windowLeft) / 1250;
        windowRight += 5 * (windowRight - windowLeft) / 1250;
        this.repaint();
    }

    /**
     * Pan window left
     */
    private void panLeft() {
        windowLeft -= 5 * (windowRight - windowLeft) / 1250;
        windowRight -= 5 * (windowRight - windowLeft) / 1250;
        this.repaint();
    }

    /**
     * Pan window down
     */
    private void panDown() {
        windowTop -= 5 * (windowTop - windowBottom) / 1250;
        windowBottom -= 5 * (windowTop - windowBottom) / 1250;
        this.repaint();
    }

    /**
     * Pan window up
     */
    private void panUp() {
        windowTop += 5 * (windowTop - windowBottom) / 1250;
        windowBottom += 5 * (windowTop - windowBottom) / 1250;
        this.repaint();
    }

    /**
     * Invoked when the mouse cursor has been moved onto a component
     * but no buttons have been pushed.
     *
     * @param e
     */
    @Override
    public void mouseMoved(MouseEvent e) {

    }

    /**
     * Invoked when a mouse button is pressed on a component and then
     * dragged.  <code>MOUSE_DRAGGED</code> events will continue to be
     * delivered to the component where the drag originated until the
     * mouse button is released (regardless of whether the mouse position
     * is within the bounds of the component).
     * <p>
     * Due to platform-dependent Drag&amp;Drop implementations,
     * <code>MOUSE_DRAGGED</code> events may not be delivered during a native
     * Drag&amp;Drop operation.
     *
     * @param e
     */
    @Override
    public void mouseDragged(MouseEvent e) {
        AffineTransform inverse;
        try {
            inverse = theVTM.createInverse();
        } catch (Exception ex) {
            System.out.println("Non-Invertible: " + ex.toString());
            return;
        }
        Point2D curMousePosition = inverse.transform(e.getPoint(), null);

        if (prevMousePosition.getX() - curMousePosition.getX() > 0)  //drag left
            panRight();
        if (prevMousePosition.getX() - curMousePosition.getX() < 0)  //drag right
            panLeft();
        if (prevMousePosition.getY() - curMousePosition.getY() > 0)  //drag down
            panUp();
        if (prevMousePosition.getY() - curMousePosition.getY() < 0)  //drag up
            panDown();
        prevMousePosition = curMousePosition;
    }

    /**
     * Invoked when the mouse wheel is rotated.
     *
     * @param e
     * @see MouseWheelEvent
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        //System.out.println(curMousePosition);
        if (theGame.isPaused())
            return;
        if (e.getWheelRotation() < 0)
            zoomIn();
        else
            zoomOut();
    }
}

