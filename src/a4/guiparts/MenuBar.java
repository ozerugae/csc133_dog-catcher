package a4.guiparts;

import a4.commands.*;

import javax.swing.*;

/**
 * JMenuBar for the game's GUI
 * Has a File & Command menu
 */
public class MenuBar extends JMenuBar {

    /**
     * Set up the Menu Bar for the game's window
     */
    public MenuBar() {
        JMenuItem mI;
        JCheckBoxMenuItem jCB;

        //Create the file menu
        JMenu fileMenu = new JMenu("File");                     //File Menu
        mI = new JMenuItem("New");
        mI.setAction(New.getInstance());
        fileMenu.add(mI);                                       //File -> New
        mI = new JMenuItem("Save");
        mI.setAction(Save.getInstance());
        fileMenu.add(mI);                                       //File -> Save
        mI = new JMenuItem("Undo");
        mI.setAction(Undo.getInstance());
        fileMenu.add(mI);                                       //File -> Undo
        jCB = new JCheckBoxMenuItem("Sound");
        jCB.setAction(Sound.getInstance());                     //bind command
        fileMenu.add(jCB);                                      //File -> Sound
        mI = new JMenuItem("About");
        mI.setAction(About.getInstance());
        fileMenu.add(mI);                                       //File -> About
        add(fileMenu);

        //Create the commandMenu
        JMenu commandMenu = new JMenu("Command");               //Command Menu
        mI = new JMenuItem("Expand Net");
        mI.setAction(Expand.getInstance());
        commandMenu.add(mI);                                    //Command -> Expand Net
        mI = new JMenuItem("Contract Net");
        mI.setAction(Contract.getInstance());
        commandMenu.add(mI);                                    //Command -> Contract Net
        commandMenu.addSeparator();
        mI = new JMenuItem("Quit");
        mI.setAction(Quit.getInstance());
        commandMenu.add(mI);                                    //Command -> Quit
        add(commandMenu);
    }
}
