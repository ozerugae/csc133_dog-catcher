package a4.guiparts;

import a4.GameWorld;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * The ScoreView JPanel. Updates the scores shown in the JLabels when
 * the model changes
 */
public class ScoreView extends JPanel implements Observer {

    private final JLabel totalPoints = new JLabel("Total Points: -1");
    private final JLabel dogsCap = new JLabel("Dogs Captured: -1");
    private final JLabel catsCap = new JLabel("Cats Captured: -1");
    private final JLabel dogsRem = new JLabel("Dogs Remaining: -1");
    private final JLabel catsRem = new JLabel("Cats Remaining: -1");
    private final JLabel sound = new JLabel("Sound: ???");

    /**
     * Set up the JPanel
     */
    public ScoreView() {
        setLayout(new GridLayout(1, 6, 10, 0));

        Border marginOuter = new EmptyBorder(2, 2, 2, 2); //outside padding
        Border border = BorderFactory.createLineBorder(new Color(64, 32, 255), 2); //border line
        Border temp = new CompoundBorder(marginOuter, border); //outside padding with border line inside
        Border margin = new EmptyBorder(10, 10, 10, 10);    //inside padding
        setBorder(new CompoundBorder(temp, margin));

        //Center all JLables
        totalPoints.setHorizontalAlignment(JLabel.CENTER);
        dogsCap.setHorizontalAlignment(JLabel.CENTER);
        catsCap.setHorizontalAlignment(JLabel.CENTER);
        dogsRem.setHorizontalAlignment(JLabel.CENTER);
        catsRem.setHorizontalAlignment(JLabel.CENTER);
        sound.setHorizontalAlignment(JLabel.CENTER);

        //Add JLabels to this Panel
        add(totalPoints);
        add(dogsCap);
        add(catsCap);
        add(dogsRem);
        add(catsRem);
        add(sound);
    }

    /**
     * This method is called whenever the observed object is changed. An
     * application calls an <tt>Observable</tt> object's
     * <code>notifyObservers</code> method to have all the object's
     * observers notified of the change.
     *
     * @param o   the observable object.
     * @param arg an argument passed to the <code>notifyObservers</code>
     */
    @Override
    public void update(Observable o, Object arg) {
        GameWorld gw = (GameWorld) o; //cast to gameworld to use gameworld's methods
        //Update relevant JLabels
        totalPoints.setText("Total Points: " + gw.getTotalPoints());
        dogsCap.setText("Dogs Captured: " + gw.getDogsCaptured());
        catsCap.setText("Cats Captured: " + gw.getCatsCaptured());
        dogsRem.setText("Dogs Remaining: " + gw.getDogsRemaining());
        catsRem.setText("Cats Remaining: " + gw.getCatsRemaining());
        sound.setText("Sound: " + (gw.getSound() ? "ON" : "OFF"));
    }
}
