package a4.sound;

import a4.Game;
import a4.GameWorld;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.*;

/**
 * From: http://www.codejava.net/coding/how-to-play-back-audio-in-java-with-examples
 * This is an example program that demonstrates how to play back an audio file
 * using the Clip in Java Sound API.
 * @author www.codejava.net
 *
 */
public class SoundClip implements LineListener {

    /**
     * this flag indicates whether the playback completes or not.
     */
    private boolean playCompleted;

    private String audioFilePath;
    private Game gameInstance = null;
    private GameWorld gameWorldInstance = null;
    private boolean audioPaused = false;
    private Clip audioClip;
    private BooleanControl volume;
    private boolean looping = false;

    /**
     * @param audioFilePath Path of the audio file.
     */
    public SoundClip(String audioFilePath){
        this.audioFilePath = audioFilePath;
        gameInstance = Game.getInstance();
        gameWorldInstance = GameWorld.getInstance();
    }

    public void play() {
        File audioFile = new File(audioFilePath);

        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
            AudioFormat format = audioStream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            audioClip = (Clip) AudioSystem.getLine(info);
            audioClip.addLineListener(this);
            audioClip.open(audioStream);

            BooleanControl volume = (BooleanControl) audioClip.getControl(BooleanControl.Type.MUTE);
            if(looping)
                audioClip.loop(Clip.LOOP_CONTINUOUSLY);
            else
                audioClip.start();

            while (!playCompleted) {
                if (!isMuted()) {                                   //If game is not muted
                    if (isGamePaused() && !audioPaused) {           //if game is paused and sound is not paused
                        pause();                                    //pause audio
                    } else if (!isGamePaused() && audioPaused) {    //if game is not paused and sound is paused
                        resume();                                   //resume audio
                    }
                    if(volume.getValue())                           //if audio is currently muted
                        volume.setValue(false);                     //unmute
                }else{                                              //if game is muted
                    if(!volume.getValue())                          //if audio is currently not muted
                        volume.setValue(true);                      //mute
                }
                // wait for the playback to complete
                try {
                    Thread.sleep(20);   //wait 20 milliseconds before looping again / supposedly matching the repaint frequency
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

            audioClip.close();

        } catch (UnsupportedAudioFileException ex) {
            System.out.println("The specified audio file is not supported.");
            ex.printStackTrace();
        } catch (LineUnavailableException ex) {
            System.out.println("Audio line for playing back is unavailable.");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error playing the audio file.");
            ex.printStackTrace();
        }

    }

    public void playLoop(){
        looping = true;
        play();
    }

    private void pause(){
        audioPaused = true;
        audioClip.stop();
    }

    private void resume(){
        audioPaused = false;
        if(looping)
            audioClip.loop(Clip.LOOP_CONTINUOUSLY);
        else
            audioClip.start();
    }

    private boolean isGamePaused(){
        return gameInstance.isPaused();
    }

    private boolean isMuted(){
        return !gameWorldInstance.getSound();
    }

    /**
     * Listens to the START and STOP events of the audio line.
     */
    @Override
    public void update(LineEvent event) {
        LineEvent.Type type = event.getType();

        if (type == LineEvent.Type.START) {
            //System.out.println("Playback started.");

        } else if (type == LineEvent.Type.STOP && !audioPaused) {
            playCompleted = true;
            //System.out.println("Playback completed.");
        }

    }
}