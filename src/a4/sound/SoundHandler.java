package a4.sound;

import java.io.File;

/**
 * All-in-one class for sounds. Plays sounds on seperate threads
 */
public class SoundHandler {

    private static volatile SoundClip catSound, dogSound, netSound, backgroundSound = null;
    private static volatile SoundHandler theSoundHandlerInstance = null;
    //System.getProperty("user.dir") + File.separator +
    private String soundDir = "." + File.separator + "sounds" + File.separator;
    private String meow = soundDir + "Meow.wav";
    private String woof0 = soundDir + "DogBark0.wav";
    private String woof1 = soundDir + "DogBark1.wav";
    private String woof2 = soundDir + "DogBark2.wav";
    private String woof3 = soundDir + "DogBark3.wav";
    private String swoosh = soundDir + "Swoosh.wav";
    private String bg = soundDir + "Background.wav";


    public static SoundHandler soundHandlerInstance() {
        if (theSoundHandlerInstance == null) {
            theSoundHandlerInstance = new SoundHandler();
        }
        return theSoundHandlerInstance;
    }

    private SoundHandler() {

    }

    public void playCat() {
        new Thread(new Runnable() {
            public void run() {
                catSound = new SoundClip(meow);
                catSound.play();
                catSound = null;
            }
        }).start();
    }

    public void playDog() {
        new Thread(new Runnable() {
            public void run() {
                switch((int)(Math.random() * 4)){
                    case 0:
                        dogSound = new SoundClip(woof0);
                        break;
                    case 1:
                        dogSound = new SoundClip(woof1);
                        break;
                    case 2:
                        dogSound = new SoundClip(woof2);
                        break;
                    case 3:
                    default:
                        dogSound = new SoundClip(woof3);
                }
                dogSound.play();
                dogSound = null;
            }
        }).start();
    }

    public void playNet() {
        new Thread(new Runnable() {
            public void run() {
                netSound = new SoundClip(swoosh);
                netSound.play();
                netSound = null;
            }
        }).start();
    }

    public void playBG() {
        new Thread(new Runnable() {
            public void run() {
                backgroundSound = new SoundClip(bg);
                backgroundSound.playLoop();
            }
        }).start();
    }
}
